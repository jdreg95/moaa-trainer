import boto3, botocore
import os
import argparse

# Get S3 folder based on env
def get_base_folder(env):
    if env == 'production':
        return 'jori/moaa/models_prod'
    elif env =='development':
        return 'jori/moaa/models_dev'
    else:
        return 'jori/moaa/models_local'

# Upload model to S3
def upload(env, path, filename, S3_KEY, S3_BUCKET, S3_SECRET):
    s3 = boto3.client('s3', aws_access_key_id=S3_KEY, aws_secret_access_key=S3_SECRET)

    with open(path, 'rb') as file:   
        s3.upload_fileobj(
            file,
            S3_BUCKET,
            '{}{}'.format(get_base_folder(env), filename)
        ) 

# Handle entry & args
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upload script to Amazon S3')
    parser.add_argument('env', type=str, metavar='env')
    parser.add_argument('path', type=str, metavar='path')
    parser.add_argument('filename', type=str, metavar='filename')
    parser.add_argument('S3_KEY', type=str, metavar='S3_KEY')
    parser.add_argument('S3_BUCKET', type=str, metavar='S3_BUCKET')
    parser.add_argument('S3_SECRET', type=str, metavar='S3_SECRET')

    args = parser.parse_args()

    upload(args.env, args.path, args.filename, args.S3_KEY, args.S3_BUCKET, args.S3_SECRET)
