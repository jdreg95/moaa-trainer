import pickle
import argparse
import matplotlib.pyplot as plt

# Read training history
def read_history(path):
    with open(path, 'rb') as handle:
        return pickle.load(handle)

# Plot graphs using training history
def plot_history(history):
    acc = history['acc']
    val_acc = history['val_acc']
    loss = history['loss']
    val_loss = history['val_loss']
    epochs = range(len(acc))

    plt.plot(epochs, acc, 'C1', label='Train accuracy', color='C1')
    plt.plot(epochs, val_acc, 'C2', label='Test accuracy', color='C2')
    plt.legend()
    plt.title('Training and validation accuracy')
    
    plt.figure()

    plt.plot(epochs, loss, 'C1', label='Train loss', color='C1')
    plt.plot(epochs, val_loss, 'C2', label='Test loss', color='C2')
    plt.legend()
    plt.title('Training and validation loss')

    plt.show()

# Main entry & args handle
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a model')
    parser.add_argument('trainingHistoryPath', type=str, help='A path for the history')
    args = parser.parse_args()
    
    history = read_history(args.trainingHistoryPath)
    plot_history(history)
