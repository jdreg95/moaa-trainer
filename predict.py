import math
import sys
import argparse
import numpy as np
from PIL import Image
import requests
from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
import plaidml.keras

# Use PlaidML backend over TF
plaidml.keras.install_backend()

from keras.preprocessing import image
from keras.models import load_model
from keras.applications.inception_v3 import preprocess_input

# Process image for prediction use
def process_image(img, target_size):
  if img.size != target_size:
    img = img.resize(target_size)

  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)
  x = preprocess_input(x)
  
  return x

# Predict binary
def predict_binary(img, model):
  prediction = model.predict(img)
  prediction = process_binary_prediction(prediction[0])
  return prediction

# Process binary prediction
def process_binary_prediction(prediction):
  p = prediction[0]
  probability = p
  return probability 


# Plot the binary prediction
def plot_binary_prediction(image, prediction):
  probability, prediction = prediction
  plt.imshow(image)
  plt.title(probability)
  plt.show()

TARGET_SIZE = (299, 299)

# Main entry & args
if __name__ == '__main__':
  a = argparse.ArgumentParser()
  a.add_argument("--image", help="path to image")
  a.add_argument("--image_url", help="url to image")
  a.add_argument("--model")
  args = a.parse_args()

  if args.image is None and args.image_url is None:
    a.print_help()
    sys.exit(1)

  model = load_model(args.model)

  if args.image is not None:
    img = Image.open(args.image)
    x = process_image(img, TARGET_SIZE)
    prediction = predict_binary(x, model)
    processed_prediction = process_binary_prediction(prediction)
    plot_binary_prediction(img, processed_prediction)

  if args.image_url is not None:
    response = requests.get(args.image_url)
    img = Image.open(BytesIO(response.content))
    x = process_image(img, TARGET_SIZE)
    prediction = predict_binary(x, model)
    processed_prediction = process_binary_prediction(prediction)
    plot_binary_prediction(img, processed_prediction)


