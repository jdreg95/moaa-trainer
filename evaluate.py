import argparse
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
import os

import matplotlib.pyplot as plt


# Get number of items in all directories
def get_num_items_in_dirs(directory):
    if not os.path.exists(directory):
        return None

    num = 0

    for root, subdirs, files in os.walk(directory):
        for dir in subdirs:
            num += get_num_items_in_dir(os.path.join(root, dir))
    
    return num

# Get number of items in one directory
def get_num_items_in_dir(directory):
    if not os.path.exists(directory):
        return 0

    num = 0

    for dir in os.listdir(directory):
        num += 1

    return num

# Evaluate a model
def evaluate(dataPath, model):
    model = load_model(model)
    num_items = get_num_items_in_dirs(dataPath)
    
    datagen = ImageDataGenerator(rescale=1./255)
    test_gen = datagen.flow_from_directory(
        dataPath,
        target_size=(299, 299),
        batch_size=32,
        class_mode='binary'
    )

    score = model.evaluate_generator(test_gen, num_items // 32)
    plot_evaluation(score)

# Plot the evaluated model scores
def plot_evaluation(score):
    objects = ('Loss (lower better)', 'Accuracy (higher better)')
    performance = [1, .80, .60, .40, .20, 0]
    loss = float(score[0] * 100)
    acc = float(score[1] * 100)
    plt.barh(objects, [loss, acc], align='center', alpha=0.5)
    plt.ylabel('Score')
    plt.title('Model loss and accuracy')
    plt.suptitle('Model accuracy: {:0.2f}%'.format(acc))

    plt.show()

# Main entry & aargs handle
if __name__ == '__main__':
    a = argparse.ArgumentParser()
    a.add_argument("dataPath", help="Path to data")
    a.add_argument("model", help="Path to model")
    args = a.parse_args()

    evaluate(args.dataPath, args.model)
